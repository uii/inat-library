#ifndef NETWORKERROR_H_INCLUDED
#define NETWORKERROR_H_INCLUDED

#include <stdexcept>
#include <string>

#include "networking.h"

// Исключение при работе с сетевым API. Автоматически определяет код ошибки
// и получает её описание. Сохраняет файл, строки и функцию, где возбуждено
// исключение (удобно это делать макросом RAISE_NETWORK_ERROR() ниже).
class NetworkError final: public std::exception
{
public:
    NetworkError(const char* place, const char* file, size_t line);
    NetworkError(const char* place, const char* file, size_t line, SOCKET sokcet);
    
    const char* what() const noexcept override;
    const char* getMessage() const noexcept;
    int getCode() const noexcept;

    const char* getPlace() const { return place_; }
    const char* getFile() const { return file_; }
    size_t getLine() const { return line_; }

private:
    int const code_;
    const std::string message_;
    const char* const place_;
    const char* const file_;
    size_t const line_;
    SOCKET _socket;
};

// Возбуждает исключение с указанием функции, файла и строки с ошибкой.
// Макрос __PRETTY_FUNCTION__ специфичен для GCC; константа __func__
// универсальна, но менее подробна.
#define RAISE_NETWORK_ERROR()                                        \
    do {                                                             \
        throw NetworkError(__PRETTY_FUNCTION__, __FILE__, __LINE__); \
    } while (0)
        
// Возбуждает исключение для ошибки, произошедшей с определенным сокетом.
#define RAISE_SOCKET_ERROR(socket)                                           \
    do {                                                                     \
        throw NetworkError(__PRETTY_FUNCTION__, __FILE__, __LINE__, socket); \
    } while (0)

#endif // NETWORKERROR_H_INCLUDED
