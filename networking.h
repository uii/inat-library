//
// Файл обеспечивает кроссплатформенность разработки.
// Содержит:
//     * включения заголовочных файлов для сетевых приложений;
//     * определения типов и констант, отсутствующих на некоторых платформах;
//     * функции-"обертки" для действий, которые выполняются по-разному.
//
#ifndef NETWORKING_H_INCLUDED
#define NETWORKING_H_INCLUDED

#ifndef __unix__

#   include <winsock2.h>
#   include <ws2tcpip.h>

#   ifndef EWOULDBLOCK
#       define EWOULDBLOCK WSAEWOULDBLOCK
#   endif

#   define NET_OS_WINDOWS

#   define close closesocket

    typedef int socklen_t;

#else

#   include <arpa/inet.h>
#   include <sys/socket.h>
#   include <unistd.h>

#   define closesocket close

#   define SD_SEND     SHUT_WR
#   define SD_RECEIVE  SHUT_RD
#   define SD_BOTH     SHUT_RDWR

#   define NET_OS_UNIX

    typedef int SOCKET;

#endif

// Разорачивает сетевую подсистему (в *nix ничего не делает).
int NET_initialize();

// Сворачивает сетевую подсистему (в *nix ничего не делает).
int NET_finalize();

// Возвращает код последней ошибки сетевых функций.
int NET_get_last_error();

// Возвращает код последней ошибки сокета (полезна в асинхронном режиме).
int NET_get_last_error(SOCKET socket);

// Возвращает описание ошибки по её коду.
const char* NET_get_error_message(int code);

// Проверяет, что результаты вызова сетевой функции означает ошибку
// (SOCKET_ERROR в Windows, отрицательное значение в *nix).
bool NET_is_error(int result);

// Проверяет, значение в переменной --- не описатель сокета
// (например, что это INVALID_SOCKET или -1).
bool NET_is_invalid(SOCKET socket);

#endif // NETWORKING_H_INCLUDED
