#include "networking.h"
#include "NetworkError.h"

NetworkError::NetworkError(const char* place, const char* file, size_t line) :
        code_{NET_get_last_error()},
        message_{NET_get_error_message(code_)},
        place_{place},
        file_{file},
        line_{line}
{
}

NetworkError::NetworkError(
    const char* place, const char* file, size_t line, SOCKET socket) :
        code_{NET_get_last_error(socket)},
        message_{NET_get_error_message(code_)},
        place_{place},
        file_{file},
        line_{line}
{
}

const char* NetworkError::what() const noexcept
{
    return getMessage();
}

const char* NetworkError::getMessage() const noexcept
{
    return message_.c_str();
}

int NetworkError::getCode() const noexcept
{
    return code_;
}

