#include "networking.h"

int NET_get_last_error(SOCKET socket)
{
    int code;
    socklen_t code_size = sizeof(code);
    ::getsockopt(socket, SOL_SOCKET, SO_ERROR, reinterpret_cast<char*>(&code), &code_size);
    return code;
}
